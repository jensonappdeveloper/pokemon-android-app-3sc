package com.example.pokemon3scjenson.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.pokemon3scjenson.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}